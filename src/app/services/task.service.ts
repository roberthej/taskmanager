import { Injectable } from '@angular/core';
import { AngularFirestore,AngularFirestoreCollection,AngularFirestoreDocument } from '@angular/fire/firestore';
import { TaskModel } from '../modes/TaskModel';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  itemsCollections: AngularFirestoreCollection<TaskModel>;
  items: Observable<TaskModel[]>;
  itemDoc: AngularFirestoreDocument<TaskModel>;

  constructor(public db: AngularFirestore) { 

    this.getAllTasks();

  }

  getAllTasks(): Observable<any> {

    this.itemsCollections = this.db.collection('task',ref=> ref.orderBy('taskCreated','desc'));
    this.items = this.itemsCollections.snapshotChanges().map(m=> {
      return m.map(a=> {
         const data = a.payload.doc.data() as TaskModel;
         data.id = a.payload.doc.id;
         return data;
      })
    })
    return this.items;
  }

  saveTask(item: TaskModel) {

    if(item.id === '') {
      this.itemsCollections.add(item);
    } else {
      this.itemDoc = this.db.doc(`task/${item.id}`);
      this.itemDoc.update(item);
    }
  }

  deleteTask(item: TaskModel) {

    this.itemDoc = this.db.doc(`task/${item.id}`);
    this.itemDoc.delete();
  }

}
