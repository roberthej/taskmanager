import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskComponent } from './components/task/task.component';



//Routes
const appRoutes:Routes = [
  { path: '', redirectTo: 'taks', pathMatch: 'full' },
  { path: 'taks', component: TaskComponent },
  { path: '**', redirectTo: '' }
];


export const routing = RouterModule.forRoot(appRoutes);
