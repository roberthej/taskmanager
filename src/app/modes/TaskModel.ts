export interface TaskModel {
    id: string;
    taskName: string;
    taskDescription: string;
    taskCreated: Date;

}