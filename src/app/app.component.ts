import { Component, HostListener, ViewChild } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { MatSidenav } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  @ViewChild('sidenavLeft') sidenavLeft: MatSidenav;
  public leftNavBarOpened: boolean;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
      if (event.target.innerWidth < 1200) {
          this.sidenavLeft.close();
      }
  }

  constructor(sanitizer: DomSanitizer,private router:Router) {
    
    this.leftNavBarOpened = true;

  }

  navclick(item){
    this.router.navigate([item]);
  }

}