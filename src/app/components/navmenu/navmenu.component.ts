import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.css']
})
export class NavmenuComponent implements OnInit {

  constructor(private router:Router) { 
  }

  ngOnInit() {
  }
  
  navclick(item){
     this.router.navigate([item]);
  }

}
