import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { TaskService } from 'src/app/services/task.service';
import { TaskModel } from 'src/app/modes/TaskModel';
import { DialogDeleteComponent } from '../dialog-delete/dialog-delete.component';
import { SelectionModel } from '@angular/cdk/collections';
import { EditTaskModalComponent } from '../edit-task-modal/edit-task-modal.component';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  displayedColumns: string[];
  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<TaskModel>(true, []);
  isRowSelected: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(public taskService: TaskService,public dialog: MatDialog,) {

    this.displayedColumns = ['select','id', 'taskName', 'taskDescription', 'taskCreated','edit','delete'];
    this.getInitData();
   }

  ngOnInit() {
    
    this.selection.onChange.subscribe(result=> {
         
         if(this.selection.selected.length > 0){
            this.isRowSelected = true
         } else {
            this.isRowSelected = false;
         }
    });
  }

  getInitData(){
    this.taskService.getAllTasks().subscribe(data => {
      this.dataSource.data = data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  gridFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  newRowForGrid() {

    let taskItem: TaskModel = {
      id: '',
      taskName: '',
      taskDescription:'',
      taskCreated: new Date
    }

    const dialogRef = this.dialog.open(EditTaskModalComponent,{
      data: taskItem,
      height: '400px',
      width:'800px'
    });

      dialogRef.afterClosed().subscribe(result => {
        
    });

  }

  editItemGrid(item: TaskModel){

    const dialogRef = this.dialog.open(EditTaskModalComponent,{
      data: item,
      height: '400px',
      width:'800px'
    });

      dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });


  }

  deleteItemGrid(item: TaskModel){
    
      let dialogRef = this.dialog.open(DialogDeleteComponent, {

        width: '400px',
        data: { answer: 'true', tekst: item.taskName, question: "Dali se sigurani da želite obrisati odabranu stavku?" }

      });

      dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined) {
          if (result) {
            this.taskService.deleteTask(item);
          }
        }

      });

  }

  deleteSelectedItemGrid(){
 
    let dialogRef = this.dialog.open(DialogDeleteComponent, {

      width: '400px',
      data: { answer: 'true', tekst: "", question: "Dali se sigurani da želite obrisati odabrane stavke?" }

    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result) {
            this.selection.selected.forEach(a=>{
                this.taskService.deleteTask(a);
            })
        }
      }
    });
  }

}
