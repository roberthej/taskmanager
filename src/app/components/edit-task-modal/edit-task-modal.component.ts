import { Component, OnInit, Inject } from '@angular/core';
import { TaskModel } from 'src/app/modes/TaskModel';
import { MAT_DIALOG_DATA, MAT_DATE_FORMATS, DateAdapter, MAT_DATE_LOCALE } from '@angular/material';
import { TaskService } from 'src/app/services/task.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';


export const DateFormat = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MMMM Y',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM Y'
  },
};

@Component({
  selector: 'app-edit-task-modal',
  templateUrl: './edit-task-modal.component.html',
  styleUrls: ['./edit-task-modal.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateFormat}
  ]
})
export class EditTaskModalComponent implements OnInit {

  taskFormGroup: FormGroup;
  task: TaskModel;

  constructor(@Inject(MAT_DIALOG_DATA) public data: TaskModel,
              public taskService: TaskService) {
                
    this.taskFormGroup = new FormGroup({
       taskName:    new FormControl(data.taskName,Validators.required),
       taskDescription: new FormControl(data.taskDescription,Validators.required),
       taskCreated: new FormControl(new Date(data.taskCreated['seconds']*1000),Validators.required)
    });

  }

  ngOnInit() {
  }

  saveTaks(){

    let itemToSave: TaskModel = {
      id: this.data.id,
      taskName: this.taskFormGroup.value.taskName,
      taskDescription: this.taskFormGroup.value.taskDescription,
      taskCreated: this.taskFormGroup.value.taskCreated._d ? this.taskFormGroup.value.taskCreated._d : this.taskFormGroup.value.taskCreated
    }
    
    this.taskService.saveTask(itemToSave);
      
  }

  public IsFieldInvalid(field: string) { 

    return (
       (!this.taskFormGroup.get(field).valid && this.taskFormGroup.get(field).touched) ||
       (this.taskFormGroup.get(field).untouched)
    );

  }

}
