import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from  '../environments/environment';
import { routing } from './app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter'



import { AngularFireModule } from  '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';


//Components
import { AppComponent } from './app.component';
import { TaskComponent } from './components/task/task.component';
import { NavmenuComponent } from './components/navmenu/navmenu.component';
import { DialogDeleteComponent } from './components/dialog-delete/dialog-delete.component';
import { EditTaskModalComponent } from './components/edit-task-modal/edit-task-modal.component';


import {  MatSidenavModule,
          MatButtonModule, 
          MatButtonToggleModule,
          MatToolbarModule,
          MatIconModule,
          MatInputModule,
          MatFormFieldModule,
          MatOptionModule,
          MatSelectModule,
          MatExpansionModule,
          MatListModule,
          MatSnackBarModule,
          MatProgressSpinnerModule,
          MatGridListModule,
          MatCheckboxModule,
          MatSlideToggleModule,
          MatCardModule,
          MatMenuModule,
          MatDialogModule,
          MatStepperModule,
          MatTooltipModule,
          MatPaginatorModule,
          MatSortModule,
          MatChipsModule,
          MatTableModule, 
          MatDatepickerModule,
          MatNativeDateModule } from '@angular/material';
          
//Services
import { TaskService } from './services/task.service';

      


@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    NavmenuComponent,
    DialogDeleteComponent,
    EditTaskModalComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfiguration),
    AngularFirestoreModule, 
    AngularFireAuthModule, 
    AngularFireStorageModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatButtonModule, 
    MatButtonToggleModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatExpansionModule,
    MatListModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatCardModule,
    MatMenuModule,
    MatDialogModule,
    MatStepperModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatSortModule,
    MatChipsModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule
  ],
  providers: [
    TaskService
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents:[
    DialogDeleteComponent,
    EditTaskModalComponent
  ]
})
export class AppModule { }
