// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfiguration: {
    apiKey: "AIzaSyCQgsQUvk_Te5p9X-rGO7vN0BNqmQRnHQk",
    authDomain: "taskdb-6b1ed.firebaseapp.com",
    databaseURL: "https://taskdb-6b1ed.firebaseio.com",
    projectId: "taskdb-6b1ed",
    storageBucket: "taskdb-6b1ed.appspot.com",
    messagingSenderId: "259064162576"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
